import ReactDOM        from 'react-dom';

import { Application } from 'sources/index';

ReactDOM.render(<Application />, document.querySelector(`#main`));
