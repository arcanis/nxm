module.exports = function (config) {

    config.set({

        browsers: [ 'PhantomJS' ],
        frameworks: [ 'mocha', 'chai-spies', 'chai' ],
        reporters: [ 'mocha' ],

        files: [
            'node_modules/babel-polyfill/browser.js',
            'sources/*.test.js',
            'sources/**/*.test.js'
        ],

        preprocessors: {
            'sources/*.test.js': [ 'webpack' ],
            'sources/**/*.test.js': [ 'webpack' ]
        },

        webpack: Object.assign({}, require('./webpack.config.js'), {
        }),

        webpackMiddleware: {
            noInfo: true
        },

        phantomjsLauncher: {
            exitOnResourceError: true
        }

    });

};
